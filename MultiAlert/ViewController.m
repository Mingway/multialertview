//
//  ViewController.m
//  MultiAlert
//
//  Created by shimingwei on 14-9-4.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "ViewController.h"
#import "MultiAlertView.h"

@interface ViewController (){
    MultiAlertView *_alert;
}

- (IBAction)showAlert:(UIButton *)sender;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)showAlert:(UIButton *)sender {
    _alert = [[MultiAlertView alloc] initMessage:@"message"];
    [_alert show];
    
    [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(addMsg) userInfo:nil repeats:YES];
}

- (void)addMsg
{
    [_alert addMsg:[[NSDate date] description]];
    if (!_alert.isOpen) {
        [_alert show];
    }
}

@end
