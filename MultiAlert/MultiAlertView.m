//
//  MultiAlertView.m
//  MultiAlert
//
//  Created by shimingwei on 14-9-4.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "MultiAlertView.h"

 #define IOS7 [[[UIDevice currentDevice]systemVersion] floatValue] >= 7.0

static const float kAlertViewWidth = 250;
static const float kAlertViewHeight = 280;

#pragma mark -
#pragma mark AlertWindow

@interface AlertWindow : UIWindow

+ (instancetype)defaultWindow;

@end

@implementation AlertWindow

+ (instancetype)defaultWindow
{
    static AlertWindow *window = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        window = [[AlertWindow alloc] initWithFrame:[UIApplication sharedApplication].keyWindow.bounds];
    });
    return window;
}

- (instancetype) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.windowLevel = UIWindowLevelAlert;
        self.backgroundColor = [UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:0.5];
    }
    return self;
}

@end

#pragma mark -
#pragma mark MultiAlertView

@interface MultiAlertView (){
    NSMutableArray *_mulMessages;
    
    UIButton *_okBtn;
    UIButton *_lastBtn;
    UIButton *_nextBtn;
    UILabel *_titleLabel;
    UITextView *_msgTextView;
    UIWindow *_keyWindow;
    
    NSInteger _currentIndex;
    BOOL _isOpen;
}

@end

@implementation MultiAlertView

- (void)dealloc
{
    [_mulMessages removeAllObjects];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype) initMessage:(NSString*)message
{
    self = [super init];
    if (self) {
        self.clipsToBounds = YES;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarFrameOrOrientationChanged:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarFrameOrOrientationChanged:) name:UIApplicationDidChangeStatusBarFrameNotification object:nil];
        
        _mulMessages = [NSMutableArray array];
        
        _okBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_okBtn addTarget:self action:@selector(hide) forControlEvents:UIControlEventTouchUpInside];
        [_okBtn setFrame:CGRectMake(0, kAlertViewHeight - 60, kAlertViewWidth, 60)];
        [_okBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_okBtn setTitle:@"OK" forState:UIControlStateNormal];
        [self addSubview:_okBtn];
        _okBtn.showsTouchWhenHighlighted = YES;
        _okBtn.layer.borderColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:0.9].CGColor;
        _okBtn.layer.borderWidth = 1;
        [_okBtn.layer setMasksToBounds:YES];
        _okBtn.layer.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.3].CGColor;
        
        _lastBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_lastBtn setFrame:CGRectMake(0, 0, 60, 60)];
        _lastBtn.tag = 1;
        [_lastBtn addTarget:self action:@selector(handleChangeMsg:) forControlEvents:UIControlEventTouchUpInside];
        [_lastBtn setImage:[UIImage imageNamed:@"leftArrow"] forState:UIControlStateNormal];
        [self addSubview:_lastBtn];
        
        _nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _nextBtn.tag = 2;
        [_nextBtn setFrame:CGRectMake(kAlertViewWidth - 60, 0, 60, 60)];
        [_nextBtn addTarget:self action:@selector(handleChangeMsg:) forControlEvents:UIControlEventTouchUpInside];
        [_nextBtn setImage:[UIImage imageNamed:@"rightArrow"] forState:UIControlStateNormal];
        [self addSubview:_nextBtn];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_lastBtn.frame), 0, CGRectGetMinX(_nextBtn.frame) - CGRectGetMaxX(_lastBtn.frame), 60)];
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = [UIFont systemFontOfSize:20.0f];
        _titleLabel.text = @"(1/1)";
        [self addSubview:_titleLabel];
        
        _msgTextView = [[UITextView alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(_titleLabel.frame) + 10, kAlertViewWidth - 10, CGRectGetMinY(_okBtn.frame) - CGRectGetMaxY(_titleLabel.frame) - 20)];
        _msgTextView.editable = NO;
        _msgTextView.selectable = NO;
        _msgTextView.backgroundColor = [UIColor clearColor];
        _msgTextView.textColor = [UIColor blackColor];
        _msgTextView.textAlignment = NSTextAlignmentCenter;
        _msgTextView.font = [UIFont systemFontOfSize:14.0f];
        [self addSubview:_msgTextView];
        _msgTextView.text = message;
        
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius = 10.0;
        self.layer.borderColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:0.9].CGColor;
        self.layer.borderWidth = 1;
        
        self.layer.shadowOpacity = 1.0f;
        self.layer.shadowOffset = CGSizeMake(10, 10);
        self.layer.shadowColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:0.9].CGColor;
        
        _currentIndex = 1;
        [self addMsg:message];
    }
    return self;
}

- (void)didChangeValueForKey:(NSString *)key
{
    if ([key isEqualToString:@"message"]) {
        [self setBtnIsEnable];
    }
}

- (void)setBtnIsEnable
{
    if (_currentIndex == 1) {
        _lastBtn.enabled = NO;
    }else{
        _lastBtn.enabled = YES;
    }
    if (_currentIndex == _mulMessages.count) {
        _nextBtn.enabled = NO;
    }else{
        _nextBtn.enabled = YES;
    }
}

- (void)contentSizeToFit {
    if([_msgTextView.text length] > 0) {
        CGSize contentSize;
        if (IOS7) {
            contentSize = [_msgTextView.text boundingRectWithSize:CGSizeMake(CGRectGetWidth(_msgTextView.frame), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : _msgTextView.font} context:nil].size;
        }else{
            contentSize = [_msgTextView.text sizeWithFont:_msgTextView.font constrainedToSize:CGSizeMake(CGRectGetWidth(_msgTextView.frame), MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
        }
        UIEdgeInsets offset = UIEdgeInsetsZero;
        CGSize newSize = contentSize;
        if(contentSize.height <= _msgTextView.frame.size.height) {
            CGFloat offsetY = (_msgTextView.frame.size.height - contentSize.height - 10)/2;
            offset = UIEdgeInsetsMake(offsetY, 0, 0, 0);
        }
        [_msgTextView setContentSize:newSize];
        [_msgTextView setContentInset:offset];
    }
}

- (void)updateTitle
{
    _titleLabel.text = [NSString stringWithFormat:@"(%li/%lu)",(long)_currentIndex,(unsigned long)_mulMessages.count];
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
}

- (void)handleChangeMsg:(UIButton*)button
{
    switch (button.tag) {
        case 1:{
            if (_currentIndex > 1) {
                _currentIndex --;
                _msgTextView.text = _mulMessages[_currentIndex-1];
            }
        }
            break;
        case 2:{
            if (_currentIndex < _mulMessages.count) {
                _currentIndex ++;
                _msgTextView.text = _mulMessages[_currentIndex-1];
            }
        }
            break;
        default:
            break;
    }
    [self contentSizeToFit];
    [self updateTitle];
    [self setBtnIsEnable];
}

- (void)addMsg:(NSString*)msg
{
    [_mulMessages addObject:msg];
    [self didChangeValueForKey:@"message"];
    [self updateTitle];
}

- (void)show
{
    if (!_isOpen) {
        _isOpen = YES;
        [self contentSizeToFit];
        
        _keyWindow = [UIApplication sharedApplication].keyWindow;
        [self rotateAccordingToStatusBarOrientationAndSupportedOrientations];
        
        for (UIView *subview in [AlertWindow defaultWindow].subviews) {
            [subview removeFromSuperview];
        }
        [[AlertWindow defaultWindow] addSubview:self];
        [[AlertWindow defaultWindow] makeKeyAndVisible];
        
        [self showAnimation];
    }
}

- (void)showAnimation
{
    _okBtn.enabled = NO;
    if (self.hidden == YES) { // swoop in if coming from hidden, otherwise pulse in-place
        self.transform = CGAffineTransformScale(self.transform,0.6,0.6);
    }
    self.hidden = NO;
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.transform = CGAffineTransformScale(self.transform,1.05,1.05);
                         self.alpha = 0.8;
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:1/15.0
                                          animations:^{
                                              self.transform = CGAffineTransformScale(self.transform,0.9,0.9);
                                              self.alpha = 0.9;
                                          }
                                          completion:^(BOOL finished) {
                                              _okBtn.enabled = YES;
                                          }
                          ];
                     }
     ];
}

- (void)hide
{
    if (_isOpen) {
        _isOpen = NO;
        [[AlertWindow defaultWindow] resignKeyWindow];
        [[AlertWindow defaultWindow] setHidden:YES];
        [_keyWindow makeKeyAndVisible];
        [self removeFromSuperview];
    }
}

- (BOOL)isOpen
{
    return _isOpen;
}

//Orientation (statusBar)

- (void)statusBarFrameOrOrientationChanged:(NSNotification *)notification
{
    /*
     This notification is most likely triggered inside an animation block,
     therefore no animation is needed to perform this nice transition.
     */
    [self rotateAccordingToStatusBarOrientationAndSupportedOrientations];
}

- (void)rotateAccordingToStatusBarOrientationAndSupportedOrientations
{
    UIInterfaceOrientation statusBarOrientation = [UIApplication sharedApplication].statusBarOrientation;
    CGFloat angle = UIInterfaceOrientationAngleOfOrientation(statusBarOrientation);
    CGFloat statusBarHeight = [[self class] getStatusBarHeight];
    
    CGAffineTransform transform = CGAffineTransformMakeRotation(angle);
    CGRect frame = [[self class] rectInWindowBounds:[UIApplication sharedApplication].keyWindow.bounds statusBarOrientation:statusBarOrientation statusBarHeight:statusBarHeight];
    
    [self setIfNotEqualTransform:transform frame:frame];
}

- (void)setIfNotEqualTransform:(CGAffineTransform)transform frame:(CGRect)frame
{
    if(!CGAffineTransformEqualToTransform(self.transform, transform))
    {
        self.transform = transform;
    }
    if(!CGRectEqualToRect(self.frame, frame))
    {
        self.frame = frame;
    }
}

+ (CGFloat)getStatusBarHeight
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(UIInterfaceOrientationIsLandscape(orientation))
    {
        return [UIApplication sharedApplication].statusBarFrame.size.width;
    }
    else
    {
        return [UIApplication sharedApplication].statusBarFrame.size.height;
    }
}

+ (CGRect)rectInWindowBounds:(CGRect)windowBounds statusBarOrientation:(UIInterfaceOrientation)statusBarOrientation statusBarHeight:(CGFloat)statusBarHeight
{
    CGRect frame;
    if (statusBarOrientation == UIDeviceOrientationPortrait || statusBarOrientation == UIDeviceOrientationPortraitUpsideDown) {
        frame = CGRectMake(CGRectGetWidth(windowBounds)/2 - kAlertViewWidth/2, CGRectGetHeight(windowBounds)/2 - kAlertViewHeight/2, kAlertViewWidth, kAlertViewHeight);
    }else{
        frame = CGRectMake(CGRectGetWidth(windowBounds)/2 - kAlertViewHeight/2, CGRectGetHeight(windowBounds)/2 - kAlertViewWidth/2, kAlertViewHeight, kAlertViewWidth);
    }
    return frame;
}

CGFloat UIInterfaceOrientationAngleOfOrientation(UIInterfaceOrientation orientation)
{
    CGFloat angle;
    
    switch (orientation)
    {
        case UIInterfaceOrientationPortraitUpsideDown:
            angle = M_PI;
            break;
        case UIInterfaceOrientationLandscapeLeft:
            angle = -M_PI_2;
            break;
        case UIInterfaceOrientationLandscapeRight:
            angle = M_PI_2;
            break;
        default:
            angle = 0.0;
            break;
    }
    
    return angle;
}

UIInterfaceOrientationMask UIInterfaceOrientationMaskFromOrientation(UIInterfaceOrientation orientation)
{
    return 1 << orientation;
}

@end
