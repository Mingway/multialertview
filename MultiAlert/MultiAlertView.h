//
//  MultiAlertView.h
//  MultiAlert
//
//  Created by shimingwei on 14-9-4.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MultiAlertView : UIView

- (instancetype)initMessage:(NSString*)message;
- (BOOL)isOpen;
- (void)show;
- (void)addMsg:(NSString*)msg;

@end


